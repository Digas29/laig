function Distrify(scene){
  this.scene = scene;
  this.gameState = [[0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0]];
  this.moveHistory = [];

  this.players = {
    black: 1,
    white: 2
  };
  this.currPlayer = this.players.black;
  this.round = 1;
  this.winner = null;

  this.timer = 0;



  this.getMoveRequest = function (requestString, port){

    var requestPort = port || 8081;
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:'+requestPort+'/'+requestString, false);
    var that = this;
    request.onload = function(data){that.handleMoveReply(data,that)};
    request.onerror = function(){console.log("Error waiting for response");};

    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send();
  };

  this.getPcRequest = function (requestString, port){

    var requestPort = port || 8081;
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:'+requestPort+'/'+requestString, true);
    var that = this;
    request.onload = function(data){that.handlePcMoveReply(data)};
    request.onerror = function(){console.log("Error waiting for response");};

    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send();
  };
}
Distrify.prototype.constructor=Distrify;
Distrify.prototype.boardToString = function(){
  var string = "[";
  for(var i = 0; i < this.gameState.length; i++){
    string += "[";
    for(var j = 0; j < this.gameState.length; j++){
      switch(this.gameState[i][j]){
      case 0:
        string += "emptyCell";
        break;
      case 1:
        string += "black";
        break;
      case 2:
        string += "white";
        break;
      }
      if(j < this.gameState.length - 1){
        string += ",";
      }
    }
    string += "]";
    if(i < this.gameState.length - 1){
      string += ",";
    }
  }
  string += "]";
  return string;
};
Distrify.prototype.sendPlayerMove = function (row, col){
  if(this.gameState[row][col] != 0){
    return;
  }

  this.moveHistory.push({row: row, col: col});
  var playerS;
  if(this.currPlayer == this.players.black){
    playerS = "black";
  }
  else{
    playerS = "white";
  }
  var cmd;
  if(this.round == 1){
    cmd = "validFirst";
  }
  else{
    cmd = "validSecond";
  }
  var request = "[" + cmd + "," + col + "," + row + "," + this.boardToString() + "," + playerS + "]";

  this.getMoveRequest(request);
}

Distrify.prototype.replay = function (row, col){
  this.rotation = false;

}
Distrify.prototype.switchPlayers = function (){
  this.scene.leaves['board'].rotate = true;
  if(this.currPlayer == this.players.black){
    this.currPlayer = this.players.white;
    this.round = 1;
  }
  else{
    this.currPlayer = this.players.black;
    this.round = 1;
  }
}
Distrify.prototype.handleMoveReply = function(data,Distrify){
  var move = Distrify.moveHistory[Distrify.moveHistory.length - 1];
  var row = move.row;
  var col = move.col;

  var res = data.target.response.split(" ");


  if(res[0] == "0"){
    Distrify.gameState[row][col] = Distrify.currPlayer;
    Distrify.scene.movePiece(Distrify.currPlayer, row, col);
    if(res[1] == "Win"){
      Distrify.winner = Distrify.currPlayer;
      return;
    }
    if(Distrify.round == 2){
      Distrify.switchPlayers();
    }
    else{
      Distrify.round += 1;
    }
  }
  else if(res[0] == "1"){
    Distrify.gameState[row][col] = Distrify.currPlayer;
    Distrify.scene.movePiece(Distrify.currPlayer, row, col);
    if(res[1] == "Win"){
      Distrify.winner = Distrify.currPlayer;
      return;
    }
    Distrify.switchPlayers();
  }
  else if(res[0] == "Valid"){
    Distrify.gameState[row][col] = Distrify.currPlayer;
    Distrify.scene.movePiece(Distrify.currPlayer, row, col);
    if(res[1] == "Win"){
      Distrify.winner = Distrify.currPlayer;
      return;
    }
    Distrify.switchPlayers();
  }
  else if(res[0] == "Invalid"){
    Distrify.moveHistory.pop();
  }
  else{
    Distrify.moveHistory.pop();
    console.log(data.target.response);
  }

}
Distrify.prototype.newGame = function (){
  this.gameState = [ [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0]];
  this.winner = null;
  this.currPlayer = this.players.black;
  this.round = 1;
}

Distrify.prototype.undo = function(){
  if(this.moveHistory.length != 0){
    var move = this.moveHistory.pop();
    var player = this.gameState[move.row][move.col];
    this.currPlayer = player;
    this.round = 1;
    if(this.moveHistory.length != 0){
      var lastMove = this.moveHistory[this.moveHistory.length-1];
      var player2 = this.gameState[lastMove.row][lastMove.col];
      if(player2 == player){
        this.round = 2;
      }
    }
    this.gameState[move.row][move.col] = 0;
    return player;
  }
  return null;
}

Distrify.prototype.playPC = function(){
  this.timer += 100;
  if(this.winner != null){
    return;
  }
  if(this.timer % 1500 == 0 && this.timer >= 1500){
    console.log(this.timer);
    var playerS;
    if(this.currPlayer == this.players.black){
      playerS = "black";
    }
    else{
      playerS = "white";
    }
    if(this.currPlayer == this.players.black && this.scene.Player1 != 'Human'){
      var type = this.scene.Player1 == "PC-Greedy" ? "greedy" : "random";
      var stringRequest = "[" + type + "," + this.boardToString() + ',' + playerS + ',' + this.round + "]";
      this.getPcRequest(stringRequest);
    }
    if(this.currPlayer == this.players.white && this.scene.Player2 != 'Human'){
      var type = this.scene.Player2 == "PC-Greedy" ? "greedy" : "random";
      var stringRequest = "[" + type + "," + this.boardToString() + ',' + playerS + ',' + this.round + "]";;
      this.getPcRequest(stringRequest);
    }
  }
}

Distrify.prototype.handlePcMoveReply = function(data){
  if(this.winner == null){
    var row = parseInt(data.target.response[1]);
    var col = parseInt(data.target.response[3]);
    this.sendPlayerMove(row,col);
  }
}
