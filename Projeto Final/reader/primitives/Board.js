function Board(scene, yMax, span, maxTime){
	CGFobject.call(this, scene);
  this.base = new MyRectangle(scene, -0.5, 0.5, 0.5, -0.5);
	this.fullBase = new MyRectangle(scene, -0.5, 0.5, 0.5, -0.5);
  this.base.updateTexCoords(1.0,1.0);

  this.boardTexture = new CGFtexture(scene, 'scenes/textures/woodboard.png');
  this.wood = new CGFtexture(scene, 'scenes/textures/wood.png');

  this.boardMaterial = new CGFappearance(scene);
  this.boardMaterial.setShininess(20.0);
  this.boardMaterial.setSpecular(0.1,0.1,0.1,1.0);
  this.boardMaterial.setAmbient(0.1,0.1,0.1,1.0);
  this.boardMaterial.setDiffuse(1.0,1.0,1.0,1.0);

  this.squareMaterial = new CGFappearance(scene);
  this.squareMaterial.setShininess(120.0);
  this.squareMaterial.setSpecular(0.0,0.0,0.0,1.0);
  this.squareMaterial.setAmbient(0.1,0.1,0.1,1.0);
  this.squareMaterial.setDiffuse(0.91,0.75,0.54,1.0);

  this.white = new CGFappearance(scene);
  this.white.setShininess(120.0);
  this.white.setSpecular(1.0,1.0,1.0,1.0);
  this.white.setAmbient(1.0,1.0,1.0,1.0);
  this.white.setDiffuse(1.0,1.0,1.0,1.0);


  this.rectangles = [];
  for(var i = 0; i < 9; i++){
    var row = [];
    for(var j = 0; j < 9; j++){
      row.push(new MyRectangle(scene, -0.5, 0.5, 0.5, -0.5));
    }
    this.rectangles.push(row);
  }

  this.blackPos = [];
  this.blackPieces = [];
  this.whitePos = [];
  this.whitePieces = [];
  var zCoord = -3.75;
  for(var i = 0; i < 4; i++){
    var yCoord = 0.5;
    var stack1 = [];
    var stack2 = [];
    var posB = [];
    var posW = [];
    for(var j = 0; j < 10; j++){
      stack1.push(new MyPiece(scene));
      stack2.push(new MyPiece(scene));
      posB.push({x: 14.5, y: yCoord, z: zCoord});
      posW.push({x: -14.5, y: yCoord, z: zCoord});
      yCoord+= 0.5;
    }
    this.blackPieces.push(stack1);
    this.whitePieces.push(stack2);
    this.blackPos.push(posB);
    this.whitePos.push(posW);
    zCoord+=2.5;
  }
  this.placedW = 0;
  this.placedB = 0;

	this.time = maxTime;
	this.miliseconds = 0;
	this.maxTime = maxTime;
	this.span = span;
	this.yMax = yMax;
	this.animationQueue = [];
	this.textWhite = new Text(scene, "WHITE:" + this.placedW.toString());
	this.textBlack = new Text(scene, "BLACK:" + this.placedB.toString());

	this.timeText = new Text(scene, "TIME:" + this.time.toString());
	this.rotate = false;
	this.ang = (Math.PI * 100) / (this.span*1000);
	this.camStart = 0;
	this.replayTime = -1;
  this.initBuffers();
};

Board.prototype = Object.create(CGFobject.prototype);
Board.prototype.constructor=Board;

Board.prototype.display = function () {
	this.scene.pushMatrix();
		this.scene.translate(5.75,0,-14);
		this.scene.rotate(Math.PI, 0,1,0);
		this.scene.rotate(-Math.PI/2, 1,0,0);
		this.scene.scale(2,2,1);
		this.timeText.display();
	this.scene.popMatrix();

	this.scene.pushMatrix();
		this.scene.translate(-5.75,0,14);
		this.scene.rotate(-Math.PI/2, 1,0,0);
		this.scene.scale(2,2,1);
		this.timeText.display();
	this.scene.popMatrix();


	this.scene.pushMatrix();
		this.scene.translate(-17.5,0,6);
		this.scene.rotate(Math.PI/2, 0,1,0);
		this.scene.rotate(-Math.PI/2, 1,0,0);
		this.scene.scale(2,2,1);
		this.textWhite.display();
	this.scene.popMatrix();
	this.scene.pushMatrix();
		this.scene.translate(17.5,0,-5.5);
		this.scene.rotate(-Math.PI/2, 0,1,0);
		this.scene.rotate(-Math.PI/2, 1,0,0);
		this.scene.scale(2,2,1);
		this.textBlack.display();
	this.scene.popMatrix();
  this.scene.pushMatrix();
    this.scene.scale(25.0, 1.0, 25.0);
    this.scene.rotate(-Math.PI/2, 1, 0, 0);
    this.boardMaterial.apply();
    this.boardTexture.bind();
    this.base.display();
    this.boardTexture.unbind();
    this.scene.setDefaultAppearance();
  this.scene.popMatrix();
  this.scene.pushMatrix();
    this.scene.translate(0.0, -0.1, 0.0);
    this.scene.scale(37.5, 1.0, 30.0);
    this.scene.rotate(-Math.PI/2, 1, 0, 0);
    this.boardMaterial.apply();
    this.wood.bind();
    this.fullBase.display();
    this.wood.unbind();
    this.scene.setDefaultAppearance();
  this.scene.popMatrix();

  this.squareMaterial.apply();
  var z = -9.46;
  for(var i = 0; i < 9; i++){
    var x = -9.46;
    for(var j = 0; j < 9; j++){

      this.scene.registerForPick((j+1)*10 + i, this.rectangles[i][j]);
      this.scene.pushMatrix();
        this.scene.translate(x, 0.01, z);
        this.scene.scale(2.3, 1.0, 2.3);
        this.scene.rotate(-Math.PI/2, 1, 0, 0);
        this.base.display();
      this.scene.popMatrix();
      x += 2.36;
    }
    z += 2.36;
  }
	this.scene.clearPickRegistration();
  this.scene.setDefaultAppearance();


  for(var i = 0; i < 4; i++){

    for(var j = 0; j < 10; j++){
      this.scene.pushMatrix();
        this.scene.translate(this.blackPos[i][j].x, this.blackPos[i][j].y, this.blackPos[i][j].z);
        this.blackPieces[i][j].display();
      this.scene.popMatrix();
      this.scene.pushMatrix();
        this.squareMaterial.apply();
        this.scene.translate(this.whitePos[i][j].x, this.whitePos[i][j].y, this.whitePos[i][j].z);
        this.whitePieces[i][j].display();
        this.scene.setDefaultAppearance();
      this.scene.popMatrix();
    }

  }
};

Board.prototype.movePiece = function(player, row, col){
	this.time = this.maxTime;
	this.miliseconds = 0;
	this.timeText.updateText("TIME:" + this.time.toString());

  var xC = -9.46 + 2.36*col;
  var yC = 0.5;
  var zC = -9.46 + 2.36*row;

  if(player == 1){
    var stack;
    var piece;
    if(this.placedB < 10){
      stack = 0;
      piece = 9 - this.placedB;
    }
    else{
      stack = parseInt(this.placedB.toString()[0]);
      piece = 9 - parseInt(this.placedB.toString()[1]);
    }
    this.placedB++;
		var xS = (xC - this.blackPos[stack][piece].x)/this.span;
		var yS = (this.yMax  - this.blackPos[stack][piece].y + this.yMax - yC)/this.span;
		var zS = (zC - this.blackPos[stack][piece].z)/this.span;
		var speed = {x: xS, y: yS, z:zS};
		this.animationQueue.push({color: player, stack: stack, piece: piece, timestamp: null, speed: speed, initialPos: this.blackPos[stack][piece]});
		this.textBlack.updateText("BLACK:" + this.placedB.toString());
  }
  else{
    var stack;
    var piece;
    if(this.placedW < 10){
      stack = 0;
      piece = 9 - this.placedW;
    }
    else{
      stack = parseInt(this.placedW.toString()[0]);
      piece = 9 - parseInt(this.placedW.toString()[1]);
    }
    this.placedW++;
		var xS = (xC - this.whitePos[stack][piece].x)/this.span;
		var yS = (this.yMax  - this.whitePos[stack][piece].y + this.yMax - yC)/this.span;
		var zS = (zC - this.whitePos[stack][piece].z)/this.span;
		var speed = {x: xS, y: yS, z:zS};
		this.animationQueue.push({color: player, stack: stack, piece: piece, timestamp: null, speed: speed, initialPos: this.whitePos[stack][piece]});
		this.textWhite.updateText("WHITE:" + this.placedW.toString());
  }
}

Board.prototype.undo = function (player) {
	if(player == 1){
		this.placedB--;
		var stack;
		var piece;
		if(this.placedB < 10){
			stack = 0;
			piece = 9 - this.placedB;
		}
		else{
			stack = parseInt(this.placedB.toString()[0]);
			piece = 9 - parseInt(this.placedB.toString()[1]);
		}
		var xS = (14.5 - this.blackPos[stack][piece].x)/this.span;
		var yS = (0.5 + piece * 0.5 - this.blackPos[stack][piece].y)/this.span;
		var zS = (-3.75 + 2.5 * stack - this.blackPos[stack][piece].z)/this.span;
		var speed = {x: xS, y: yS, z:zS};
		this.animationQueue.push({color: player, stack: stack, piece: piece, timestamp: null, speed: speed, initialPos: this.blackPos[stack][piece]});
		this.textBlack.updateText("BLACK:" + this.placedB.toString());
	}
	else if(player == 2){
		this.placedW--;
		var stack;
		var piece;
		if(this.placedW < 10){
			stack = 0;
			piece = 9 - this.placedW;
		}
		else{
			stack = parseInt(this.placedW.toString()[0]);
			piece = 9 - parseInt(this.placedW.toString()[1]);
		}
		var xS = (-14.5 - this.whitePos[stack][piece].x)/this.span;
		var yS = (0.5 + piece * 0.5 - this.whitePos[stack][piece].y)/this.span;
		var zS = (-3.75 + 2.5 * stack - this.whitePos[stack][piece].z)/this.span;
		var speed = {x: xS, y: yS, z:zS};
		this.animationQueue.push({color: player, stack: stack, piece: piece, timestamp: null, speed: speed, initialPos: this.whitePos[stack][piece]});
		this.textWhite.updateText("WHITE:" + this.placedW.toString());
	}
}

Board.prototype.newGame = function () {
	this.time = this.maxTime;
	this.miliseconds = 0;
	this.replayTime = -1;

	while(this.placedB != 0){
		this.placedB--;
		var stack;
		var piece;
		if(this.placedB < 10){
			stack = 0;
			piece = 9 - this.placedB;
		}
		else{
			stack = parseInt(this.placedB.toString()[0]);
			piece = 9 - parseInt(this.placedB.toString()[1]);
		}
		var xC = 14.5;
		var yC = 0.5 + piece * 0.5;
		var zC = -3.75 + 2.5 * stack;
		this.blackPos[stack][piece] = {x: xC, y: yC, z:zC};
	}

	while(this.placedW != 0){
		this.placedW--;
		var stack;
		var piece;
		if(this.placedW < 10){
			stack = 0;
			piece = 9 - this.placedW;
		}
		else{
			stack = parseInt(this.placedW.toString()[0]);
			piece = 9 - parseInt(this.placedW.toString()[1]);
		}
		var xC = -14.5;
		var yC = 0.5 + piece * 0.5;
		var zC = -3.75 + 2.5 * stack;
		this.whitePos[stack][piece] = {x: xC, y: yC, z:zC};
	}

	if(this.replayTime != -1)
		this.timeText.updateText("TIME:" + this.time.toString());

	this.textWhite.updateText("WHITE:" + this.placedW.toString());
	this.textBlack.updateText("BLACK:" + this.placedB.toString());
}

Board.prototype.replay = function () {

	this.replayTime = 0;


	while(this.placedB != 0){
		this.placedB--;
		var stack;
		var piece;
		if(this.placedB < 10){
			stack = 0;
			piece = 9 - this.placedB;
		}
		else{
			stack = parseInt(this.placedB.toString()[0]);
			piece = 9 - parseInt(this.placedB.toString()[1]);
		}
		var xC = 14.5;
		var yC = 0.5 + piece * 0.5;
		var zC = -3.75 + 2.5 * stack;
		this.blackPos[stack][piece] = {x: xC, y: yC, z:zC};
	}

	while(this.placedW != 0){
		this.placedW--;
		var stack;
		var piece;
		if(this.placedW < 10){
			stack = 0;
			piece = 9 - this.placedW;
		}
		else{
			stack = parseInt(this.placedW.toString()[0]);
			piece = 9 - parseInt(this.placedW.toString()[1]);
		}
		var xC = -14.5;
		var yC = 0.5 + piece * 0.5;
		var zC = -3.75 + 2.5 * stack;
		this.whitePos[stack][piece] = {x: xC, y: yC, z:zC};
	}

	this.timeText.updateText("REPLAY");
	this.textWhite.updateText("WHITE:" + this.placedW.toString());
	this.textBlack.updateText("BLACK:" + this.placedB.toString());
}
Board.prototype.animate = function (t) {

	if(this.replayTime == -1 && this.animationQueue.length == 0 && this.scene.game.winner != null){
		this.replay();
	}
	if(this.rotate && this.scene.RotationEnable){
		this.camStart += 100;
		this.scene.camera.orbit([0, 1, 0], this.ang);
		if(this.camStart == this.span * 1000){
			this.rotate = false;
			this.camStart = 0;
		}
	}
	else{
		this.rotate = false;
		this.miliseconds += 100;
	}

	if(this.miliseconds % 1000 == 0 && this.miliseconds >= 1000 && this.scene.game.winner == null){
		this.time -= 1;
		this.timeText.updateText("TIME:" + this.time.toString());
		if(this.time == 0){
			this.scene.game.switchPlayers();
			this.time = this.maxTime;
			this.miliseconds = 0;
		}
	}
	if(this.replayTime >= 0 && this.animationQueue.length == 0){
		this.replayTime += 100;
		if(this.replayTime % 1500 == 0 && this.replayTime >= 1500){
			if(this.scene.game.moveHistory.length != 0){
				var move = this.scene.game.moveHistory.shift();
				this.movePiece(this.scene.game.gameState[move.row][move.col], move.row, move.col);
			}
		}
	}
	var deleteIndex = null;
	for(var i = 0; i < this.animationQueue.length; i++){
		var stack = this.animationQueue[i].stack;
		var piece = this.animationQueue[i].piece;
		var color = this.animationQueue[i].color;
		if(this.animationQueue[i].timestamp == null){
			this.animationQueue[i].timestamp = t;
		}
		else{
			var time = Math.min(this.span, t - this.animationQueue[i].timestamp);
			var speed = this.animationQueue[i].speed;
			var xC = this.animationQueue[i].initialPos.x + time * speed.x;
			var yC = this.animationQueue[i].yTime == undefined ? this.animationQueue[i].initialPos.y + time * speed.y : this.yMax - (time - this.animationQueue[i].yTime) * speed.y;
			var zC = this.animationQueue[i].initialPos.z + time * speed.z;
			if(yC >= this.yMax){
				this.animationQueue[i].yTime = time;
				yC = this.yMax;
			}
			if(time == this.span){
				yC = 0.5;
			}
			if(color == 1){
				this.blackPos[stack][piece] = ({x: xC, y: yC, z: zC});
			}
			else{
				this.whitePos[stack][piece] = ({x: xC, y: yC, z: zC});
			}
			if(time == this.span){
				deleteIndex = i;
			}
		}
	}
	if(deleteIndex != null){
		this.animationQueue.splice(0, deleteIndex + 1);
	}
}
