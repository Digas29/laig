/**
* @class Plane
* @constructor
* @param {CGFScene} cena da primitiva
* @param {integer} numero de partes por eixo
*/
function Text(scene, text) {
	CGFobject.call(this,scene);

  this.text = text;
  this.shader = new CGFshader(scene.gl, "scenes/shaders/font.vert", "scenes/shaders/font.frag");
  this.shader.setUniformsValues({'dims': [16, 16]});

  this.appearance = new CGFappearance(scene);
  this.appearance.setAmbient(0.3, 0.3, 0.3, 1);
  this.appearance.setDiffuse(0.7, 0.7, 0.7, 1);
  this.appearance.setSpecular(0.0, 0.0, 0.0, 1);
  this.appearance.setShininess(120);

  this.fontTexture = new CGFtexture(scene, "scenes/textures/oolite-font.png");
  this.appearance.setTexture(this.fontTexture);

  this.base = new MyRectangle(scene, -0.5, 0.5, 0.5, -0.5);
  this.base.updateTexCoords(1.0,1.0);
  this.row = [];
  this.row[' '] = 2;
  this.row[':'] = 3;
  this.row['0'] = 3;
  this.row['1'] = 3;
  this.row['2'] = 3;
  this.row['3'] = 3;
  this.row['4'] = 3;
  this.row['5'] = 3;
  this.row['6'] = 3;
  this.row['7'] = 3;
  this.row['8'] = 3;
  this.row['9'] = 3;
  this.row['A'] = 4;
  this.row['B'] = 4;
  this.row['C'] = 4;
  this.row['D'] = 4;
  this.row['E'] = 4;
  this.row['F'] = 4;
  this.row['G'] = 4;
  this.row['H'] = 4;
  this.row['I'] = 4;
  this.row['J'] = 4;
  this.row['K'] = 4;
  this.row['L'] = 4;
  this.row['M'] = 4;
  this.row['N'] = 4;
  this.row['O'] = 4;
  this.row['P'] = 5;
  this.row['Q'] = 5;
  this.row['R'] = 5;
  this.row['S'] = 5;
  this.row['T'] = 5;
  this.row['U'] = 5;
  this.row['V'] = 5;
  this.row['W'] = 5;
  this.row['X'] = 5;
  this.row['Y'] = 5;
  this.row['Z'] = 5;

  this.col = [];
  this.col[' '] = 0;
  this.col[':'] = 10;
  this.col['0'] = 0;
  this.col['1'] = 1;
  this.col['2'] = 2;
  this.col['3'] = 3;
  this.col['4'] = 4;
  this.col['5'] = 5;
  this.col['6'] = 6;
  this.col['7'] = 7;
  this.col['8'] = 8;
  this.col['9'] = 9;
  this.col['A'] = 1;
  this.col['B'] = 2;
  this.col['C'] = 3;
  this.col['D'] = 4;
  this.col['E'] = 5;
  this.col['F'] = 6;
  this.col['G'] = 7;
  this.col['H'] = 8;
  this.col['I'] = 9;
  this.col['J'] = 10;
  this.col['K'] = 11;
  this.col['L'] = 12;
  this.col['M'] = 13;
  this.col['N'] = 14;
  this.col['O'] = 15;
  this.col['P'] = 0;
  this.col['Q'] = 1;
  this.col['R'] = 2;
  this.col['S'] = 3;
  this.col['T'] = 4;
  this.col['U'] = 5;
  this.col['V'] = 6;
  this.col['W'] = 7;
  this.col['X'] = 8;
  this.col['Y'] = 9;
  this.col['Z'] = 10;
	this.initBuffers();
};

Text.prototype = Object.create(CGFobject.prototype);
Text.prototype.constructor=Text;

Text.prototype.updateText = function (text) {
  this.text = text;
}
Text.prototype.display = function () {
  this.appearance.apply();
  this.scene.setActiveShaderSimple(this.shader);

  for(var i = 0; i < this.text.length; i++){
		this.scene.pushMatrix();
    var col = this.col[this.text[i]];
    var row = this.row[this.text[i]];
    this.scene.activeShader.setUniformsValues({'charCoords': [col,row]});
		this.scene.translate(i,0,0);
		this.base.display();
		this.scene.popMatrix();
  }
  this.scene.setActiveShaderSimple(this.scene.defaultShader);
  this.scene.setDefaultAppearance();
}
