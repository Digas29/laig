/**
* MyPiece
* @constructor
*/

function MyPiece(scene) {
	CGFobject.call(this,scene);
	this.cylinder = new MyCylinder(this.scene,0.5, 1, 1, 30, 30);
	this.circle = new MyCircle(this.scene,30);
	this.initBuffers();
};

MyPiece.prototype =Object.create(CGFobject.prototype);
MyPiece.prototype.constructor=MyPiece;

MyPiece.prototype.display=function(){
	this.scene.pushMatrix();
	  	this.scene.rotate(Math.PI/2, 1, 0, 0);
	    this.cylinder.display();
	this.scene.popMatrix();
    this.scene.pushMatrix();
    	this.scene.rotate(-Math.PI/2, 1, 0, 0);
        this.circle.display();
    this.scene.popMatrix();
    this.scene.pushMatrix();
    	this.scene.translate(0,-0.5,0);
    	this.scene.rotate(Math.PI/2, 1, 0, 0);
      this.circle.display();
    this.scene.popMatrix();
}
