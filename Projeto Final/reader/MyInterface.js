/**
 * @class MyInterface
 * @constructor
 */


function MyInterface() {
	//call CGFinterface constructor
	CGFinterface.call(this);
};

MyInterface.prototype = Object.create(CGFinterface.prototype);
MyInterface.prototype.constructor = MyInterface;

/**
 * init
 * @param {CGFapplication} application
 */
MyInterface.prototype.init = function(application) {
	// call CGFinterface init
	CGFinterface.prototype.init.call(this, application);

	// init GUI. For more information on the methods, check:
	//  http://workshop.chromeexperiments.com/examples/gui

	this.gui = new dat.GUI();

	this.group = this.gui.addFolder("Lights");

	this.lights = [];

	return true;
};

MyInterface.prototype.addLightToFolder = function(light) {
	this.lights[this.lights.length]= this.group.add(light, "enable").name(light.id);
}

MyInterface.prototype.addGameButtons = function(buttons) {
	this.undo = this.gui.add(buttons, "Undo");
	this.new = this.gui.add(buttons, "NewGame");
	this.switch = this.gui.add(buttons, "SwitchScene");
}
MyInterface.prototype.addPlayers = function() {
	this.player1 = this.gui.add(this.scene, "Player1", ['Human', 'PC-Random', 'PC-Greedy']);
	this.player2 = this.gui.add(this.scene, "Player2", ['Human', 'PC-Random', 'PC-Greedy']);
}
MyInterface.prototype.addRotation = function() {
	this.enable = this.gui.add(this.scene, "RotationEnable");
}

MyInterface.prototype.removeAll = function() {
	this.gui.remove(this.enable);
	this.gui.remove(this.player1);
	this.gui.remove(this.player2);
	this.gui.remove(this.undo);
	this.gui.remove(this.new);
	this.gui.remove(this.switch);
	for(var i = 0; i < this.lights.length; i++)
		this.group.remove(this.lights[i]);
	this.lights = [];
}
