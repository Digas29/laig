<!--In the following, these symbols have the significance:

    ii: integer value
    ff: float value
    ss: string value
    cc: character "x" or "y" or "z"
    tt: "0" or "1" with Boolean significance
-->
<SCENE>


	<INITIALS>
		<frustum near="0.1" far="500"/>                      <!-- frustum planes-->
		<translation x="0" y="0" z="0" />                 <!-- initial translate -->
		<rotation axis="x" angle="0" />                  <!-- initial rotation 3 -->
		<rotation axis="y" angle="0" />                  <!-- initial rotation 2 -->
		<rotation axis="z" angle="0" />                  <!-- initial rotation 1 -->
		<scale sx="0.5" sy="0.5" sz="0.5" />                  <!-- initial scaling -->
		<reference length="0" />                          <!-- axis length; "0" means no axis-->
	</INITIALS>


	<ILLUMINATION>
		<ambient r="0.1" g="0.1" b="0.1" a="1.0" />           <!-- global ambient -->
		<background r="1" g="1" b="1" a="1"/>         <!-- background color -->
	</ILLUMINATION>


	<LIGHTS>
		<LIGHT id="lightCeiling">
			<enable value="1"/>
			<position x="10" y="9.9" z="10" w="1" />
			<ambient r="0.9" g="0.9" b="0.9" a="1.0" />
			<diffuse r="0.99" g="0.99" b="0.99" a="1.0" />
			<specular r="0.9" g="0.9" b="0.9" a="1.0" />
		</LIGHT>
		<LIGHT id="lightLamp">
			<enable value="1"/>
			<position x="5" y="4.5" z="5" w="1" />
			<ambient r="0.3" g="0.3" b="0.3" a="1.0" />
			<diffuse r="0.9" g="0.9" b="0.9" a="1.0" />
			<specular r="0.9" g="0.9" b="0.9" a="1.0" />
		</LIGHT>
		<LIGHT id="lightBoard">                                    <!-- light identifier -->
      <enable value="1" />                         <!-- enable/disable -->
      <position x="10" y="2.2" z="10" w="1.0" />       <!-- light position -->
      <ambient r="0.1" g="0.1" b="0.1" a="1.0" />        <!-- ambient component -->
      <diffuse r="0.9" g="0.9" b="0.9" a="1.0" />        <!-- diffuse component -->
      <specular r="0.0" g="0.0" b="0.0" a="1.0" />       <!-- specular component -->
    </LIGHT>
	</LIGHTS>


	<TEXTURES>
		<TEXTURE id="textureFloor">
			<file path="resources/wood-floor.png" />
			<amplif_factor s="2" t="2" />
		</TEXTURE>
		<TEXTURE id="textureWood">
			<file path="resources/table.png" />
			<amplif_factor s="2" t="1" />
		</TEXTURE>
		<TEXTURE id="textureMetal">
			<file path="resources/metal.png" />
			<amplif_factor s="1" t="1" />
		</TEXTURE>
		<TEXTURE id="textureWindow">
			<file path="resources/window.png"/>
			<amplif_factor s="2" t="1" />
		</TEXTURE>
		<TEXTURE id="textureWall">
			<file path="resources/wall.png"/>
			<amplif_factor s="2" t="1" />
		</TEXTURE>
		<TEXTURE id="textureCarpet">
			<file path="resources/carpet.png"/>
			<amplif_factor s="2" t="1" />
		</TEXTURE>
		<TEXTURE id="textureTVfront">
			<file path="resources/TVfront.png"/>
			<amplif_factor s="2" t="1" />
		</TEXTURE>
		<TEXTURE id="textureSofaRed">
			<file path="resources/sofaRed.png"/>
			<amplif_factor s="2" t="1" />
		</TEXTURE>
		<TEXTURE id="textureSofaBlack">
			<file path="resources/sofaBlack.png"/>
			<amplif_factor s="2" t="1" />
		</TEXTURE>
		<TEXTURE id="textureLamp">
			<file path="resources/lamp.png"/>
			<amplif_factor s="1" t="1" />
		</TEXTURE>
		<TEXTURE id="textureLeaf">
			<file path="resources/leaf.png"/>
			<amplif_factor s="1" t="1" />
		</TEXTURE>
		<TEXTURE id="texturePot">
			<file path="resources/pot.png"/>
			<amplif_factor s="1" t="1" />
		</TEXTURE>
		<TEXTURE id="textureTrunk">
			<file path="resources/wood.png"/>
			<amplif_factor s="1" t="1" />
		</TEXTURE>
		<TEXTURE id="textureDirt">
			<file path="resources/dirt.png"/>
			<amplif_factor s="1" t="1"/>
		</TEXTURE>


		<!-- NOTE: this block "TEXTURE" must be repeated as necessary with different "id" -->
	</TEXTURES>


	<MATERIALS>
		<MATERIAL id="materialDefault">
			<shininess value= "1" />
			<specular r="0" g="0" b="0" a="1" />
			<diffuse r="0.5" g="0.5" b="0.5" a="1" />
			<ambient r="0.0" g="0.0" b="0.0" a="1" />
			<emission r="0.0" g="0.0" b="0.0" a="1" />
		</MATERIAL>

		<MATERIAL id="materialWood">
			<shininess value="80" />
			<specular r="0.1" g="0.1" b="0.1" a="1" />        <!-- specular reflection -->
			<diffuse r="0.8" g="0.8" b="0.8" a="1" />         <!-- diffuse reflection -->
			<ambient r="0.3" g="0.3" b="0.3" a="1" />         <!-- ambient reflection -->
			<emission r="0.3" g="0.3" b="0.3" a="1" />        <!-- emission component -->
		</MATERIAL>

		<MATERIAL id="materialMetal">
			<shininess value="80" />
			<specular r="0.83" g="0.83" b="0.83" a="1" />        <!-- specular reflection -->
			<diffuse r="0.61" g="0.61" b="0.61" a="1" />         <!-- diffuse reflection -->
			<ambient r="0.31" g="0.31" b="0.31" a="1" />         <!-- ambient reflection -->
			<emission r="1" g="1" b="1" a="1" />        <!-- emission component -->
		</MATERIAL>

		<MATERIAL id="materialWall">
			<shininess value= "1" />
			<specular r="0.1" g="0.1" b="0.1" a="1" />
			<diffuse r="0.5" g="0.5" b="0.5" a="1" />
			<ambient r="0.5" g="0.5" b="0.5" a="1" />
			<emission r="0.3" g="0.3" b="0.3" a="1" />
		</MATERIAL>


		<!-- NOTE: the "MATERIAL" block may be repeated as required. Each defined material requires a distinct "id" -->
	</MATERIALS>


	<LEAVES>
		<!-- next lines define nodes of type leaf; they may be repeated, in any order, as necessary -->
		<LEAF id="rectangle" type="rectangle" args="-1 0.5 1 -0.5" />      <!-- 2D coordinates for left-top and right-bottom vertices. -->
		<LEAF id="cylinder" type="cylinder" args="1 1 1 12 12"/>    <!-- height, bottom radius, top radius, sections along height, parts per section -->
		<LEAF id="cone" type="cylinder" args="1 1 0 12 12"/>  <!-- height, bottom radius, top radius, sections along height, parts per section -->
		<LEAF id="lampShade" type="cylinder" args="1 1 0.5 12 12"/>  <!-- height, bottom radius, top radius, sections along height, parts per section -->
		<LEAF id="sphere" type="sphere" args="1 12 12" />            <!-- radius, parts along radius, parts per section -->
		<LEAF id="triangle" type="triangle" args="-1 0 0 1 0 0 0 1 0" />   <!-- coordinates of each vertex -->
		<LEAF id="board" type="board" />   <!-- coordinates of each vertex -->
	</LEAVES>

<animations>

</animations>

	<NODES>
		<ROOT id="scene" />     <!-- identifier of root node of the scene graph; this node     -->
		<!--   must be defined in one of the following NODE declarations -->
		<NODE id="scene">       <!--   defines one intermediate node; may be repeated as necessary -->

			<!-- next two lines are mandatory -->
			<MATERIAL id="materialDefault" />      <!-- declared material superimposes the material received from parent node -->
			<!-- id="null" maintains material from parent node        -->
			<TEXTURE id="null" />       <!-- declared texture superimposes the texture received from parent node -->
			<!-- id="null" maintains texture from parent node       -->
			<!-- id="clear" clears texture declaration received from parent node      -->

			<!-- declaring descendents, ate least one node or one leaf must be present -->
			<DESCENDANTS>
				<DESCENDANT id="floor"/>
				<DESCENDANT id="wallTop"/>
				<DESCENDANT id="wallBack"/>
				<DESCENDANT id="wallLeft"/>
				<DESCENDANT id="wallRight"/>
				<DESCENDANT id="table"/>
				<DESCENDANT id="carpet"/>
				<DESCENDANT id="couchBig"/>
				<DESCENDANT id="couchSmall"/>
				<DESCENDANT id="television"/>
				<DESCENDANT id="lamp"/>
				<DESCENDANT id="tab"/>
				<DESCENDANT id="ceilingLight"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="floor">
			<MATERIAL id="materialWood" />
			<TEXTURE id="textureFloor" />
			<TRANSLATION x="10" y="0" z="10" />
			<ROTATION axis="x" angle="-90" />
			<SCALE sx="10" sy="20" sz="0.1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="wallTop">
			<MATERIAL id="materialWall" />
			<TEXTURE id="textureWall" />
			<TRANSLATION x="10" y="10" z="10" />
			<ROTATION axis="x" angle="90" />
			<SCALE sx="10" sy="20" sz="0.1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="wallBack">
			<MATERIAL id="materialWall" />
			<TEXTURE id="textureWindow" />
			<TRANSLATION x="10" y="5" z="0" />
			<SCALE sx="10" sy="10" sz="0.1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="wallLeft">
			<MATERIAL id="materialWall" />
			<TEXTURE id="textureWall" />
			<TRANSLATION x="0" y="5" z="10" />
			<ROTATION axis="y" angle="90" />
			<SCALE sx="10" sy="10" sz="0.1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="wallRight">
			<MATERIAL id="materialWall" />
			<TEXTURE id="textureWall" />
			<TRANSLATION x="20" y="5" z="10" />
			<ROTATION axis="y" angle="-90" />
			<SCALE sx="10" sy="10" sz="0.1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="ceilingLight">
			<MATERIAL id="materialMetal"/>
			<TEXTURE id="textureMetal"/>
			<TRANSLATION x="10" y="10" z="10"/>
			<ROTATION axis="x" angle="90"/>
			<SCALE sx="1" sy="1" sz="0.1"/>
			<DESCENDANTS>
				<DESCENDANT id="sphere"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="table">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="10" y="0.2" z="10" />
			<ROTATION axis="y" angle="90"/>
			<ROTATION axis="x" angle="-90" />
			<SCALE sx="1.5" sy="1.5" sz="1" />
			<DESCENDANTS>
				<DESCENDANT id="tableLegs" />
				<DESCENDANT id="tableTop" />
				<DESCENDANT id="tableBack"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="tableTop">
			<MATERIAL id="materialWood" />
			<TEXTURE id="textureWood" />
			<TRANSLATION x="0" y="0" z="2" />
			<SCALE sx="2" sy="2" sz="1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="tableBack">
			<MATERIAL id="materialWood" />
			<TEXTURE id="textureWood" />
			<TRANSLATION x="0" y="0" z="2" />
			<ROTATION axis="x" angle="180"/>
			<SCALE sx="2" sy="2" sz="1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="tableLegs">
			<MATERIAL id="materialMetal"/>
			<TEXTURE id="textureMetal"/>
			<SCALE sx="0.1" sy="0.1" sz="2" />
			<DESCENDANTS>
				<DESCENDANT id="tableLeg1" />
				<DESCENDANT id="tableLeg2" />
				<DESCENDANT id="tableLeg3" />
				<DESCENDANT id="tableLeg4" />
			</DESCENDANTS>
		</NODE>

		<NODE id="tableLeg1">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="-18" y="8" z="0" />
			<DESCENDANTS>
				<DESCENDANT id="cylinder" />
			</DESCENDANTS>
		</NODE>
		<NODE id="tableLeg2">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="18" y="8" z="0" />
			<DESCENDANTS>
				<DESCENDANT id="cylinder" />
			</DESCENDANTS>
		</NODE>
		<NODE id="tableLeg3">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="18" y="-8" z="0" />
			<DESCENDANTS>
				<DESCENDANT id="cylinder" />
			</DESCENDANTS>
		</NODE>
		<NODE id="tableLeg4">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="-18" y="-8" z="0" />
			<DESCENDANTS>
				<DESCENDANT id="cylinder" />
			</DESCENDANTS>
		</NODE>

		<NODE id="carpet">
			<MATERIAL id="materialWood" />
			<TEXTURE id="textureCarpet" />
			<TRANSLATION x="10" y="0.1" z="10" />
			<ROTATION axis="x" angle="-90" />
			<SCALE sx="5" sy="10" sz="0.1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>


		<NODE id="television">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="20" y="5" z="6" />
			<ROTATION axis="y" angle="-90"/>
			<SCALE sx="4" sy="4" sz="0.1" />
			<DESCENDANTS>
				<DESCENDANT id="televisionFront"/>
				<DESCENDANT id="televisionBack"/>
				<DESCENDANT id="televisionTop"/>
				<DESCENDANT id="televisionBottom"/>
				<DESCENDANT id="televisionLeft"/>
				<DESCENDANT id="televisionRight"/>
			</DESCENDANTS>
		</NODE>
		<NODE id="televisionFront">
			<MATERIAL id="materialMetal" />
			<TEXTURE id="textureTVfront" />
			<TRANSLATION x="1" y="0.5" z="1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="televisionBack">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="0.5" z="0" />
			<ROTATION axis="x" angle="180" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="televisionTop">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="1" z="0.5" />
			<ROTATION axis="x" angle="-90" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="televisionBottom">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="0" z="0.5" />
			<ROTATION axis="x" angle="90" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="televisionLeft">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="0" y="0.5" z="0.5" />
			<ROTATION axis="y" angle="-90" />
			<SCALE sx="0.5" sy="1" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="televisionRight">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="2" y="0.5" z="0.5" />
			<ROTATION axis="y" angle="90" />
			<SCALE sx="0.5" sy="1" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="couchBig">
			<MATERIAL id="null"/>
			<TEXTURE id="textureSofaBlack"/>
			<ROTATION axis="y" angle="90"/>
			<TRANSLATION x="-12" y="0.1" z="5"/>
			<DESCENDANTS>
				<DESCENDANT id="couchBack"/>
				<DESCENDANT id="couchBody"/>
				<DESCENDANT id="couchLeft"/>
				<DESCENDANT id="couchRight"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="couchSmall">
			<MATERIAL id="null"/>
			<TEXTURE id="textureSofaRed"/>
			<TRANSLATION x="9" y="0.1" z="5"/>
			<SCALE sx="0.5" sy="1" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="couchBack"/>
				<DESCENDANT id="couchBody"/>
				<DESCENDANT id="couchLeft"/>
				<DESCENDANT id="couchRight"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="couchBack">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="-1" y="0" z="-1"/>
			<SCALE sx="3" sy="2" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="couchSideFront"/>
				<DESCENDANT id="couchSideBack"/>
				<DESCENDANT id="couchSideTop"/>
				<DESCENDANT id="couchSideBottom"/>
				<DESCENDANT id="couchSideLeft"/>
				<DESCENDANT id="couchSideRight"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="couchBody">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<SCALE sx="2" sy="1" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="couchBodyFront"/>
				<DESCENDANT id="couchBodyTop"/>
				<DESCENDANT id="couchBodyBottom"/>
			</DESCENDANTS>
		</NODE>
		<NODE id="couchBodyFront">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="0.5" z="1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="couchBodyTop">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="1" z="0.5" />
			<ROTATION axis="x" angle="-90" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="couchBodyBottom">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="0" z="0.5" />
			<ROTATION axis="x" angle="90" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="couchLeft">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="4" y="0" z="0"/>
			<SCALE sx="0.5" sy="2" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="couchSideFront"/>
				<DESCENDANT id="couchSideBack"/>
				<DESCENDANT id="couchSideTop"/>
				<DESCENDANT id="couchSideBottom"/>
				<DESCENDANT id="couchSideLeft"/>
				<DESCENDANT id="couchSideRight"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="couchRight">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="-1" y="0" z="0"/>
			<SCALE sx="0.5" sy="2" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="couchSideFront"/>
				<DESCENDANT id="couchSideBack"/>
				<DESCENDANT id="couchSideTop"/>
				<DESCENDANT id="couchSideBottom"/>
				<DESCENDANT id="couchSideLeft"/>
				<DESCENDANT id="couchSideRight"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="couchSideFront">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="0.5" z="1" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="couchSideBack">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="0.5" z="0" />
			<ROTATION axis="x" angle="180" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="couchSideTop">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="1" z="0.5" />
			<ROTATION axis="x" angle="-90" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="couchSideBottom">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="1" y="0" z="0.5" />
			<ROTATION axis="x" angle="90" />
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="couchSideLeft">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="0" y="0.5" z="0.5" />
			<ROTATION axis="y" angle="-90" />
			<SCALE sx="0.5" sy="1" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>
		<NODE id="couchSideRight">
			<MATERIAL id="null" />
			<TEXTURE id="null" />
			<TRANSLATION x="2" y="0.5" z="0.5" />
			<ROTATION axis="y" angle="90" />
			<SCALE sx="0.5" sy="1" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="rectangle" />
			</DESCENDANTS>
		</NODE>

		<NODE id="lamp">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="5" y="0" z="5"/>
			<DESCENDANTS>
				<DESCENDANT id="lampSphere"/>
				<DESCENDANT id="lampLampShade"/>
				<DESCENDANT id="lampBody"/>
			</DESCENDANTS>
		</NODE>
		<NODE id="lampSphere">
			<MATERIAL id="materialWood"/>
			<TEXTURE id="textureLamp"/>
			<TRANSLATION x="0" y="5" z="0"/>
			<SCALE sx="0.2" sy="0.2" sz="0.2"/>
			<DESCENDANTS>
				<DESCENDANT id="sphere"/>
			</DESCENDANTS>
		</NODE>

		<NODE id="lampLampShade">
			<MATERIAL id="materialWood"/>
			<TEXTURE id="textureLamp"/>
			<TRANSLATION x="0" y="4.5" z="0"/>
			<ROTATION axis="x" angle="-90"/>
			<SCALE sx="1" sy="1" sz="1"/>
			<DESCENDANTS>
				<DESCENDANT id="lampShade"/>
			</DESCENDANTS>
		</NODE>


		<NODE id="lampBody">
			<MATERIAL id="materialWood"/>
			<TEXTURE id="textureWood"/>
			<ROTATION axis="x" angle="-90"/>
			<SCALE sx="0.3" sy="0.3" sz="5"/>
			<DESCENDANTS>
				<DESCENDANT id="cone"/>
			</DESCENDANTS>
		</NODE>
		<NODE id="tab">
			<MATERIAL id="null"/>
			<TEXTURE id="null"/>
			<TRANSLATION x="10" y="2.3" z="10"/>
			<SCALE sx="0.079" sy="0.079" sz="0.079"/>
			<DESCENDANTS>
				<DESCENDANT id="board"/>
			</DESCENDANTS>
		</NODE>


	</NODES>
</SCENE>
