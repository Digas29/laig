function XMLscene(interface) {
    CGFscene.call(this);
    this.interface = interface;
}

XMLscene.prototype = Object.create(CGFscene.prototype);
XMLscene.prototype.constructor = XMLscene;

XMLscene.prototype.init = function (application) {
    CGFscene.prototype.init.call(this, application);

    this.initCameras();
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);

    this.gl.clearDepth(100.0);
    this.gl.enable(this.gl.DEPTH_TEST);
    this.gl.enable(this.gl.CULL_FACE);
    this.gl.depthFunc(this.gl.LEQUAL);

    this.enableTextures(true);
    this.started = false;
    this.timer = 0;
    this.setUpdatePeriod(100);
    this.axis = new CGFaxis(this);

    this.filenames =
    this.name = new Text(this,"DISTRIFY");
    this.winnerMsg = new Text(this,"BLACK WINS");
    this.setPickEnabled(true);

};


XMLscene.prototype.initCameras = function () {
    this.camera = new CGFcamera(0.4, 0.1, 500, vec3.fromValues(15, 15, 15), vec3.fromValues(0, 0, 0));
};
XMLscene.prototype.movePiece = function (player, row, col)
{
  this.leaves['board'].movePiece(player,row,col);
};


XMLscene.prototype.logPicking = function ()
{
	if (this.pickMode == false) {
		if (this.pickResults != null && this.pickResults.length > 0) {
			for (var i=0; i< this.pickResults.length; i++) {
				var obj = this.pickResults[i][0];
				if (obj && this.game.winner == null &&
          ((this.game.currPlayer == this.game.players.black && this.Player1 == "Human") ||
        (this.game.currPlayer == this.game.players.white && this.Player2 == "Human")) && this.pickResults[i][1] < 200)
				{
					var customId = this.pickResults[i][1];
          var col = parseInt(customId.toString()[0]) - 1 ;
          var row = parseInt(customId.toString()[1]);
				  this.game.sendPlayerMove(row,col);
				}
			}
			this.pickResults.splice(0,this.pickResults.length);
		}
	}
};

XMLscene.prototype.setDefaultAppearance = function () {
    this.setAmbient(0.1, 0.1, 0.1, 1.0);
    this.setDiffuse(0.1, 0.1, 0.1, 1.0);
    this.setSpecular(0.1, 0.1, 0.1, 1.0);
    this.setShininess(10.0);
};

// Handler called when the graph is finally loaded.
// As loading is asynchronous, this may be called already after the application has started the run loop
XMLscene.prototype.onGraphLoaded = function (){
	//INITIALS BLOCK

	this.initialsTrans = this.graph.initials.transformationMatrix;
	this.axis = new CGFaxis(this, this.graph.initials.axisLength);
  this.camera.near = this.graph.initials.frustumNear;
  this.camera.far = this.graph.initials.frustumFar;

	//ILLUMINATON BLOCK
	this.gl.clearColor(this.graph.background.r,this.graph.background.g,this.graph.background.b,this.graph.background.a);
	this.setGlobalAmbientLight(this.graph.ambient.r,this.graph.ambient.g,this.graph.ambient.b,this.graph.ambient.a);


	//LIGHTS BLOCK
	for(var i = 0; i < this.graph.lights.length; i++){
		this.lights[i].setPosition(this.graph.lights[i].position.x, this.graph.lights[i].position.y, this.graph.lights[i].position.z,this.graph.lights[i].position.w);
		this.lights[i].setAmbient(this.graph.lights[i].ambient.r, this.graph.lights[i].ambient.g, this.graph.lights[i].ambient.b,this.graph.lights[i].ambient.a);
		this.lights[i].setDiffuse(this.graph.lights[i].diffuse.r, this.graph.lights[i].diffuse.g, this.graph.lights[i].diffuse.b,this.graph.lights[i].diffuse.a);
		this.lights[i].setSpecular(this.graph.lights[i].specular.r, this.graph.lights[i].specular.g, this.graph.lights[i].specular.b,this.graph.lights[i].specular.a);
		this.lights[i].setVisible(false);
		if(this.graph.lights[i].enable == true)
			this.lights[i].enable();
      this.interface.addLightToFolder(this.graph.lights[i]);
	}

	//TEXTURE BLOCK

	this.textures = [];
	this.textInfo = [];
	for(var i = 0; i < this.graph.textures.length; i++){
		this.textures[this.graph.textures[i].id] = new CGFtexture(this, this.graph.textures[i].path);
		this.textInfo[this.graph.textures[i].id] = this.graph.textures[i];
	}

	//MATERIALS BLOCK

	this.materials = [];
	for(var i = 0; i < this.graph.materials.length; i++){
		var material = new CGFappearance(this);
		material.setShininess(this.graph.materials[i].shininess);
		material.setSpecular(this.graph.materials[i].specular.r,this.graph.materials[i].specular.g,
		this.graph.materials[i].specular.b, this.graph.materials[i].specular.a);
		material.setDiffuse(this.graph.materials[i].diffuse.r,this.graph.materials[i].diffuse.g,
		this.graph.materials[i].diffuse.b, this.graph.materials[i].diffuse.a);
		material.setAmbient(this.graph.materials[i].ambient.r,this.graph.materials[i].ambient.g,
		this.graph.materials[i].ambient.b, this.graph.materials[i].ambient.a);
		material.setEmission(this.graph.materials[i].emission.r,this.graph.materials[i].emission.g,
		this.graph.materials[i].emission.b, this.graph.materials[i].emission.a);
		this.materials[this.graph.materials[i].id] = material;
	}

	//LEAVES BLOCK
	this.leaves = [];
  this.leavesProperties = [];
  for (var i = 0; i < this.graph.primitives.length; i++) {
    this.leavesProperties[this.graph.primitives[i].id] = this.graph.primitives[i];
    switch (this.graph.primitives[i].type) {
      case "rectangle":
        this.leaves[this.graph.primitives[i].id] = new MyRectangle(this, this.graph.primitives[i].args[0], this.graph.primitives[i].args[1],
          this.graph.primitives[i].args[2], this.graph.primitives[i].args[3]);
        break;
      case "cylinder":
      this.leaves[this.graph.primitives[i].id] = new MyCylinder(this, this.graph.primitives[i].args[0], this.graph.primitives[i].args[1],
        this.graph.primitives[i].args[2], this.graph.primitives[i].args[3], this.graph.primitives[i].args[4]);
        break;
      case "sphere":
      this.leaves[this.graph.primitives[i].id] = new MySphere(this, this.graph.primitives[i].args[0], this.graph.primitives[i].args[1],
        this.graph.primitives[i].args[2]);
        break;
      case "triangle":
      this.leaves[this.graph.primitives[i].id] = new MyTriangle(this, this.graph.primitives[i].args[0], this.graph.primitives[i].args[1],
        this.graph.primitives[i].args[2], this.graph.primitives[i].args[3], this.graph.primitives[i].args[4], this.graph.primitives[i].args[5], this.graph.primitives[i].args[6],
          this.graph.primitives[i].args[7], this.graph.primitives[i].args[8]);
        break;
      case "plane":
        this.leaves[this.graph.primitives[i].id] = new Plane(this, this.graph.primitives[i].args[0]);
        break;
      case "circle":
        this.leaves[this.graph.primitives[i].id] = new MyCircle(this.this.graph.primitives[i].args[0]);
        break;
      case "patch":
        this.leaves[this.graph.primitives[i].id] = new Patch(this, this.graph.primitives[i].args[0], this.graph.primitives[i].args[4],
          this.graph.primitives[i].args[2], this.graph.primitives[i].args[3]);
        break;
      case "vehicle":
        this.leaves[this.graph.primitives[i].id] = new Vehicle(this);
        break;
      case "piece":
        this.leaves[this.graph.primitives[i].id] = new MyPiece(this);
        break;
      case "board":
        this.leaves[this.graph.primitives[i].id] = new Board(this, 8.0, 1.5, 15.0);
        this.game = new Distrify(this);
        this.Player1 = 'Human';
        this.Player2 = 'Human';
        this.RotationEnable = false;
        var that = this;
        var buttons = {
          Undo: function (){
            var player = that.game.undo();
            if(player != null)
              that.leaves['board'].undo(player);
          },
          NewGame:  function (){
            that.leaves['board'].newGame();
            that.game.newGame();
          },
          SwitchScene: function (){
            that.graph.loadedOk = null;
            that.graph.fullFileName = that.graph.fullFileName == "scenes/scene.lsx"? "scenes/scene2.lsx":"scenes/scene.lsx";
            that.graph.reader.open( that.graph.fullFileName, that.graph);
            that.interface.removeAll();
          }
        };
        this.interface.addPlayers();
        this.interface.addRotation();
        this.interface.addGameButtons(buttons);
        break;
      case "terrain":
        this.leaves[this.graph.primitives[i].id] = new Terrain(this, this.graph.primitives[i].args[0], this.graph.primitives[i].args[1]);
        break;
      default:
      break;
    }
  }

  //NODES
  this.nodeRoot = this.graph.root;
  this.nodes = this.graph.nodes;

  this.camera.setPosition(vec3.fromValues(5, 5, 8))
  this.camera.setTarget(vec3.fromValues(5, 1.15, 5));
};

XMLscene.prototype.display = function () {
  this.logPicking();
  this.clearPickRegistration();

	// Clear image and depth buffer everytime we update the scene
    this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

	// Initialize Model-View matrix as identity (no transformation
	this.updateProjectionMatrix();
    this.loadIdentity();

    this.pushMatrix();
    if(this.game != undefined){
    	this.translate(-0.5,1.5,-8);
      this.scale(0.15,0.15,1);
      this.name.display();
      this.popMatrix();
      this.pushMatrix();
      if(this.game.winner != null){
        this.translate(-0.6,1.5,-8);
        this.scale(0.15,0.15,1);
        this.winnerMsg.display();
      }
    }
    this.popMatrix();
	// Apply transformations corresponding to the camera position relative to the origin
	this.applyViewMatrix();

	// Draw axis

	this.setDefaultAppearance();

	// ---- END Background, camera and axis setup

	// it is important that things depending on the proper loading of the graph
	// only get executed after the graph has loaded correctly.
	// This is one possible way to do it

	if (this.graph.loadedOk)
	{
    this.multMatrix(this.initialsTrans);
    this.axis.display();
		for(var i = 0; i < this.graph.lights.length; i++){
			this.lights[i].update();
		}
		this.drawNodes(this.nodeRoot, this.nodes[this.nodeRoot].transformations,this.nodes[this.nodeRoot].material, this.nodes[this.nodeRoot].texture);
    this.updateLights();
	}
  else{
    this.axis.display();
  }

};
/**
* Recursive function to draw the scene loaded from the .lsx file
* @param {string} id of node being processed
* @param {mat4} matrix with current transformations
* @param {string} id of the material of the current node
* @param {string} id of the texture of the current node
*/
XMLscene.prototype.drawNodes = function(node, matrix, material, texture){
	var nodeInfo = this.nodes[node];

	var materialNode = nodeInfo.material;
	var textureNode = nodeInfo.texture;

	if(materialNode == "null"){
		materialNode = material;
	}
	if(textureNode == "null"){
		textureNode = texture;
	}
  else if(textureNode == "clear"){
    textureNode = "null";
  }

	var geoTrans = nodeInfo.transformations;
  var newMatrix = mat4.create();
  mat4.multiply(newMatrix, matrix, geoTrans);

  if(nodeInfo.animations.length != 0)
    mat4.multiply(newMatrix, newMatrix, this.getAnimationMatrix(nodeInfo));
	var descendants = nodeInfo.descendants;



	for(var i = 0; i < descendants.length; i++){
		var descendant = descendants[i];
		if(this.leaves[descendant] == null){
			this.drawNodes(descendant, newMatrix, materialNode, textureNode);
		}
		else{
			this.drawPrimitive(descendant, newMatrix, materialNode, textureNode);
		}
	}
};
/**
* Draws a primitive
* @param {string} id of primitive being drawed
* @param {mat4} matrix with current transformations
* @param {string} id of the material of the primitive
* @param {string} id of the texture of the primitive
*/
XMLscene.prototype.drawPrimitive = function(primitive, matrix, material, textureName){

    this.pushMatrix();
        this.multMatrix(matrix);
        if(material != "null"){
          this.materials[material].apply();
        }
        var texture;
        if(textureName != "null"){
          this.textures[textureName].bind();
          if(this.leavesProperties[primitive].type == "rectangle" || this.leavesProperties[primitive].type == "triangle"){
            this.leaves[primitive].updateTexCoords(this.textInfo[textureName].ampS, this.textInfo[textureName].ampT);
          }
        }
        this.leaves[primitive].display();
    this.popMatrix();
    this.setDefaultAppearance();
    if(textureName != "null"){
      this.textures[textureName].unbind;
    }

}
/**
* Update Lights from the interface values
*/
XMLscene.prototype.updateLights = function(){
  for (var i = 0; i < this.graph.lights.length; i++) {
    if(this.graph.lights[i].enable){
      this.lights[i].enable();
    }
    else{
      this.lights[i].disable();
    }
  }
}
XMLscene.prototype.update = function(time){
  if(!this.started && this.graph.loadedOk){
    this.started = true;
    this.initTime = time;
  }
  this.timer = (time - this.initTime) / 1000;
  if(this.graph.loadedOk && this.game != undefined){
    this.leaves['board'].animate(this.timer);
    this.game.playPC();
  }
}
/**
* Get's the transformation matrix from a node animation
* @param {Node} node information
* @return {mat4} transformations matrix
*/
XMLscene.prototype.getAnimationMatrix = function(node){
  if(node.animations.length == 0)
    return;
  var id = node.animations[node.animNum];

  var animation = this.graph.animations[id];
  var animationMatrix = animation.calMatrix(this.timer);
  if(this.graph.animations[id].done){
    if(node.animNum < node.animations.length - 1){
      node.animNum++;
      this.timer = 0;
      this.started = false;
    }
  }
  return animationMatrix;
}
